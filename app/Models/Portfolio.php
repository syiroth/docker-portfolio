<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Portfolio extends Model
{
    use HasFactory;

    const PATH_THUMBNAIL = 'portfolio/thumbnail';

    protected $table = 'portfolios';
    protected $fillable = [
        'title',
        'slug',
        'description',
        'category_id',
        'thumbnail',
        'video',
        'client',
    ];

    protected $appends = [
        'path_thumbnail'
    ];

    public function getPathThumbnailAttribute() {
        return asset('storage/' . $this->thumbnail);
    }

    /**
     * Get the category that owns the Portfolio
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
