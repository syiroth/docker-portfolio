<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    const PATH_THUMBNAIL = 'service/thumbnail';

    protected $table = 'services';
    protected $fillable = [
        'title',
        'slug',
        'sub_title',
        'thumbnail',
        'description',
    ];

    protected $appends = [
        'path_thumbnail'
    ];

    public function getPathThumbnailAttribute() {
        return asset('storage/' . $this->thumbnail);
    }
}
