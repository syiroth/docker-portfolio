<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends Model
{
    use HasFactory;

    const PATH_THUMBNAIL = 'post/thumbnail';

    protected $table = 'posts';
    protected $fillable = [
        'user_id',
        'title',
        'slug',
        'description',
        'category_id',
        'thumbnail',
    ];

    protected $appends = [
        'path_thumbnail'
    ];

    public function getPathThumbnailAttribute() {
        return asset('storage/' . $this->thumbnail);
    }

    public function user(): BelongsTo 
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
