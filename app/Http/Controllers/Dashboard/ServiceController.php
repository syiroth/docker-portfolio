<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Service;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    public function index() {
        $services = Service::orderBy('id', 'desc')->get();
        return response()->json([
            'data' => [
                'services' => $services
            ]
        ], 200);
    }

    public function store(Request $request) {
        $data = $request->all();
        $data['slug'] = Str::slug($data['title'], '-');

        $validator = Validator::make($data, [
            'title' => ['required', 'string', 'max:255'],
            'slug' => ['required', 'unique:services', 'max:255'],
            'description' => ['required'],
            'sub_title' => ['required', 'string', 'max:255'],
            'thumbnail' => ['required', 'image'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 422); // 422 adalah error validations
        }

        $validated = $validator->validated();

        if ($request->has('thumbnail')) {
            $path_thumbnail = Storage::disk('public')->put(Service::PATH_THUMBNAIL, $validated['thumbnail']);
        }

        $service = Service::create([
            'title' => $validated['title'],
            'slug' => $validated['slug'],
            'description' => $validated['description'],
            'sub_title' => $validated['sub_title'],
            'thumbnail' => $path_thumbnail,
        ]);

        return response()->json([
            'data' => [
                'service' => $service
            ],
        ], 200);
    }

    public function show($serviceId) {
        $service = Service::findOrFail($serviceId);

        return response()->json([
            'data' => [
                'service' => $service
            ],
        ], 200);
    }

    public function update(Request $request, $serviceId) {
        $data = $request->all();
        $data['slug'] = Str::slug($data['title'], '-');

        $validator = Validator::make($data, [
            'title' => ['required', 'string', 'max:255'],
            'slug' => ['required', Rule::unique('services')->ignore($serviceId), 'max:255'],
            'description' => ['required'],
            'sub_title' => ['required', 'string', 'max:255'],
            'thumbnail' => ['nullable', 'image'],
        ]);

        return response()->json([
            'errors' => $validator->errors()
        ], 422); // 422 adalah error validations

        $validated = $validator->validated();

        $service = Service::findOrFail($serviceId);

        if ($request->has('thumbnail')) {
            if($service->thumbnail != null) Storage::delete($service->thumbnail);
            $path_thumbnail = Storage::disk('public')->put(Service::PATH_THUMBNAIL, $validated['thumbnail']);
        }

        $service->update([
            'title' => $validated['title'],
            'slug' => $validated['slug'],
            'description' => $validated['description'],
            'thumbnail' => $path_thumbnail ?? $service->thumbnail,
            'sub_title' => $validated['sub_title'],
        ]);

        return response()->json([
            'data' => [
                'service' => $service
            ],
        ], 200);
    }

    public function destroy($serviceId) {
        $service = Service::findOrFail($serviceId);
        Storage::delete($service->thumbnail);
        $service->delete();
        
        return response()->json([
            'message' => 'services successfully deleted!',
        ], 200);
    }
}
