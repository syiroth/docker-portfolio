<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Testimonial;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TestimonialController extends Controller
{
    public function index() {
        $testimonials = Testimonial::orderBy('id', 'desc')->get();
        return response()->json([
            'data' => [
                'testimonials' => $testimonials
            ]
        ], 200);
    }

    public function store(Request $request) {
        $data = $request->all();
        $data['slug'] = Str::slug($data['name'], '-');

        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:255', 'unique:testimonials'],
            'title' => ['required', 'string', 'max:255'],
            'slug' => ['required', 'unique:testimonials', 'max:255'],
            'description' => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 422); // 422 adalah error validations
        }

        $validated = $validator->validated();

        $testimonial = Testimonial::create([
            'name' => $validated['name'],
            'title' => $validated['title'],
            'slug' => $validated['slug'],
            'description' => $validated['description'],
        ]);

        return response()->json([
            'data' => [
                'testimonial' => $testimonial
            ],
        ], 200);
    }

    public function show($testimonialId) {
        $testimonial = Testimonial::findOrFail($testimonialId);

        return response()->json([
            'data' => [
                'testimonial' => $testimonial
            ],
        ], 200);
    }

    public function update(Request $request, $testimonialId) {
        $data = $request->all();
        $data['slug'] = Str::slug($data['name'], '-');

        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:255', Rule::unique('testimonials')->ignore($testimonialId), 'max:255'],
            'title' => ['required', 'string', 'max:255'],
            'slug' => ['required', Rule::unique('testimonials')->ignore($testimonialId), 'max:255'],
            'description' => ['required'],
        ]);

        return response()->json([
            'errors' => $validator->errors()
        ], 422); // 422 adalah error validations

        $validated = $validator->validated();

        $testimonial = Testimonial::findOrFail($testimonialId);
        $testimonial->update([
            'name' => $validated['name'],
            'title' => $validated['title'],
            'slug' => $validated['slug'],
            'description' => $validated['description'],
        ]);

        return response()->json([
            'data' => [
                'testimonial' => $testimonial
            ],
        ], 200);
    }

    public function destroy($testimonialId) {
        $testimonial = Testimonial::findOrFail($testimonialId);
        $testimonial->delete();
        
        return response()->json([
            'message' => 'testimonials successfully deleted!',
        ], 200);
    }
}
