<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Portfolio;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PortfolioController extends Controller
{
    public function index() {
        $portfolios = Portfolio::with(['category'])->orderBy('id', 'desc')->get();
        return response()->json([
            'data' => [
                'portfolios' => $portfolios
            ]
        ], 200);
    }

    public function store(Request $request) {
        $data = $request->all();
        $data['slug'] = Str::slug($data['title'], '-');

        $validator = Validator::make($data, [
            'title' => ['required', 'string', 'max:255'],
            'category_id' => ['required', 'integer', 'exists:categories,id'],
            'slug' => ['required', 'unique:portfolios', 'max:255'],
            'description' => ['required'],
            'thumbnail' => ['nullable', 'image'],
            'video' => ['nullable', 'string', 'max:255'],
            'client' => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 422); // 422 adalah error validations
        }

        $validated = $validator->validated();

        if ($request->has('thumbnail')) {
            $path_thumbnail = Storage::disk('public')->put(Portfolio::PATH_THUMBNAIL, $validated['thumbnail']);
        }

        $portfolio = Portfolio::create([
            'title' => $validated['title'],
            'category_id' => $validated['category_id'],
            'slug' => $validated['slug'],
            'description' => $validated['description'],
            'thumbnail' => $path_thumbnail ?? null,
            'video' => $validated['video'] ?? null,
            'client' => $validated['client'],
        ]);

        return response()->json([
            'data' => [
                'portfolio' => $portfolio
            ],
        ], 200);
    }

    public function show($portfolioId) {
        $portfolio = Portfolio::with(['category'])->findOrFail($portfolioId);

        return response()->json([
            'data' => [
                'portfolio' => $portfolio
            ],
        ], 200);
    }

    public function update(Request $request, $portfolioId) {
        $data = $request->all();
        $data['slug'] = Str::slug($data['title'], '-');


        $validator = Validator::make($data, [
            'title' => ['required', 'string', 'max:255'],
            'category_id' => ['required', 'integer', 'exists:categories,id'],
            'slug' => ['required', Rule::unique('portfolios')->ignore($portfolioId), 'max:255'],
            'description' => ['required'],
            'thumbnail' => ['nullable', 'image'],
            'video' => ['nullable', 'string', 'max:255'],
            'client' => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 422); // 422 adalah error validations
        }

        $validated = $validator->validated();
        $portfolio = Portfolio::findOrFail($portfolioId);

        if ($request->has('thumbnail')) {
            if($portfolio->thumbnail != null) Storage::delete($portfolio->thumbnail);
            
            $path_thumbnail = Storage::disk('public')->put(Portfolio::PATH_THUMBNAIL, $validated['thumbnail']);
        }

        $portfolio->update([
            'title' => $validated['title'],
            'category_id' => $validated['category_id'],
            'slug' => $validated['slug'],
            'description' => $validated['description'],
            'thumbnail' => $path_thumbnail ?? $portfolio->thumbnail,
            'video' => $validated['video'] ?? null,
            'client' => $validated['client'],
        ]);

        return response()->json([
            'data' => [
                'portfolio' => $portfolio
            ],
        ], 200);
    }

    public function destroy($portfolioId) {
        $portfolio = Portfolio::findOrFail($portfolioId);
        Storage::delete($portfolio->thumbnail);
        $portfolio->delete();

        return response()->json([
            'message' => 'Portfolios successfully deleted!',
        ], 200);
    }
}
