<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Post;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function index() {
        $this->authorize('viewAny', Post::class);

        // jika role admin maka tampilkan semua, jika role writer maka hanya milik dia yang dikembalikan
        $user = request()->user();
        $roles_name = $user->roles()->pluck('name')->toArray();

        $posts = Post::when(in_array('writer', $roles_name), function(Builder $query) use ($user){
                $query->where('user_id', $user->id);
            })
            ->with(['user', 'category'])
            ->orderBy('id', 'desc')
            ->get();

        return response()->json([
            'data' => [
                'posts' => $posts
            ]
            ], 200);
    }

    public function store(Request $request) {
        $this->authorize('create', Post::class);

        $data = $request->all();
        $data['slug'] = Str::slug($data['title'], '-');

        $validator = Validator::make($data, [
            'title' => ['required', 'string', 'max:255', 'unique:posts'],
            'category_id' => ['required', 'integer', 'exists:categories,id'],
            'slug' => ['required', 'unique:posts', 'max:255'],
            'description' => ['required'],
            'thumbnail' => ['required', 'image'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 422); // 422 adalah error validations
        }

        $validated = $validator->validated();

        if ($request->has('thumbnail')) {
            $path_thumbnail = Storage::disk('public')->put(Post::PATH_THUMBNAIL, $validated['thumbnail']);
        }

        $post = Post::create([
            'user_id' => $request->user()->id,
            'title' => $validated['title'],
            'category_id' => $validated['category_id'],
            'slug' => $validated['slug'],
            'description' => $validated['description'],
            'thumbnail' => $path_thumbnail ?? null,
        ]);

        return response()->json([
            'data' => [
                'post' => $post
            ],
        ], 200);
    }

    public function show($postId) {
        $post = Post::with(['user', 'category'])->findOrFail($postId);

        $this->authorize('view', $post);

        return response()->json([
            'data' => [
                'post' => $post
            ],
        ], 200);
    }

    public function update(Request $request, $postId) {
        $post = Post::findOrFail($postId);
        $this->authorize('update', $post);

        $data = $request->all();
        $data['slug'] = Str::slug($data['title'], '-');

        $validator = Validator::make($data, [
            'title' => ['required', 'string', 'max:255', 'unique:posts'],
            'category_id' => ['required', 'integer', 'exists:categories,id'],
            'slug' => ['required', Rule::unique('posts')->ignore($postId), 'max:255'],
            'description' => ['required'],
            'thumbnail' => ['nullable', 'image'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 422); // 422 adalah error validations
        }

        $validated = $validator->validated();

        if ($request->has('thumbnail')) {
            if($post->thumbnail != null) Storage::delete($post->thumbnail);
            
            $path_thumbnail = Storage::disk('public')->put(Post::PATH_THUMBNAIL, $validated['thumbnail']);
        }

        $post->update([
            'title' => $validated['title'],
            'category_id' => $validated['category_id'],
            'slug' => $validated['slug'],
            'description' => $validated['description'],
            'thumbnail' => $path_thumbnail ?? $post->thumbnail,
        ]);

        return response()->json([
            'data' => [
                'post' => $post
            ],
        ], 200);
    }

    public function destroy($postId) {
        $post = Post::findOrFail($postId);
        $this->authorize('delete', $post);

        Storage::delete($post->thumbnail);

        $post->delete();

        return response()->json([
            'message' => 'Post successfully deleted!',
        ], 200);
    }
}
