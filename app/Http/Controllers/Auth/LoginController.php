<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email'],
            'password' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 422); // 422 adalah error validations
        }

        $validated = $validator->validated();

        // cek emailnya ada ga
        $user = User::where('email', $request->email)->firstOrFail();

        // cek passwordnya
        if (! Hash::check($request->password, $user->password)) {
            return response()->json([
                'errors' => [
                    'password' => 'The provided password does not match our records.'
                ]
            ], 401);
        }

        // if (! Auth::attempt([
        //     'email' => $request->email,
        //     'password' => $request->password,
        // ])) {
        //     return response()->json([
        //         'errors' => 'The given data was invalid'
        //     ], 401);
        // }

        $token = $user->createToken('my-app-token')->plainTextToken;
        
        return response()->json([
            'data' => [
                'user' => $user,
                'token' => $token,
            ]
        ], 200);
    }
    
    /**
     * logout
     *
     * @param  mixed $request
     * @return void
     */
    public function destroy(Request $request) {
        $user = $request->user();
        // $user = auth()->user();

        $user->tokens()->delete();

        return response()->json([
            'message' => 'Logged out'
        ], 200);
    }
}
