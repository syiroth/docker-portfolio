<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;

use App\Models\Portfolio;
use App\Models\Post;
use App\Models\Service;
use App\Models\Testimonial;
use App\Policies\PortfolioPolicy;
use App\Policies\PostPolicy;
use App\Policies\ServicePolicy;
use App\Policies\TestimonialPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        Post::class => PostPolicy::class,
        Portfolio::class => PortfolioPolicy::class,
        Service::class => ServicePolicy::class,
        Testimonial::class => TestimonialPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        //
    }
}
