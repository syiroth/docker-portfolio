<?php

use App\Http\Controllers\Dashboard\PortfolioController;
use App\Http\Controllers\Dashboard\PostController;
use App\Http\Controllers\Dashboard\ServiceController;
use App\Http\Controllers\Dashboard\TestimonialController;
use Illuminate\Support\Facades\Route;


// public routes

// protected routes
Route::middleware('auth:sanctum')->prefix('dashboard')->group(function(){
    // portfolios
    Route::get('portfolios', [PortfolioController::class, 'index'])->name('portfolios.index');
    Route::post('portfolios', [PortfolioController::class, 'store'])->name('portfolios.store');
    Route::get('portfolios/{portfolioId}', [PortfolioController::class, 'show'])->name('portfolios.show');
    Route::post('portfolios/{portfolioId}', [PortfolioController::class, 'update'])->name('portfolios.update');
    Route::delete('portfolios/{portfolioId}', [PortfolioController::class, 'destroy'])->name('portfolios.destroy');
    
    // posts
    Route::get('posts', [PostController::class, 'index'])->name('posts.index');
    Route::post('posts', [PostController::class, 'store'])->name('posts.store');
    Route::get('posts/{postId}', [PostController::class, 'show'])->name('posts.show');
    Route::post('posts/{postId}', [PostController::class, 'update'])->name('posts.update');
    Route::delete('posts/{postId}', [PostController::class, 'destroy'])->name('posts.destroy');

    // services
    Route::get('services', [ServiceController::class, 'index'])->name('services.index');
    Route::post('services', [ServiceController::class, 'store'])->name('services.store');
    Route::get('services/{serviceId}', [ServiceController::class, 'show'])->name('services.show');
    Route::post('services/{serviceId}', [ServiceController::class, 'update'])->name('services.update');
    Route::delete('services/{serviceId}', [ServiceController::class, 'destroy'])->name('services.destroy');

    // testimonials
    Route::get('testimonials', [TestimonialController::class, 'index'])->name('testimonials.index');
    Route::post('testimonials', [TestimonialController::class, 'store'])->name('testimonials.store');
    Route::get('testimonials/{testimonialId}', [TestimonialController::class, 'show'])->name('testimonials.show');
    Route::post('testimonials/{testimonialId}', [TestimonialController::class, 'update'])->name('testimonials.update');
    Route::delete('testimonials/{testimonialId}', [TestimonialController::class, 'destroy'])->name('testimonials.destroy');
});

require __DIR__.'/auth.php';