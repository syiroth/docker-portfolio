<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use Illuminate\Support\Facades\Route;


// register
Route::post('register', [RegisterController::class, 'store'])->name('register');
Route::post('login', [LoginController::class, 'store'])->name('login');

Route::middleware('auth:sanctum')->group(function(){
    Route::post('logout', [LoginController::class, 'destroy'])->name('logout');
});