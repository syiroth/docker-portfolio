<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $users_id = User::pluck('id')->toArray();
        $categories_id = Category::pluck('id')->toArray();
        $title = fake()->sentence();
        $slug = Str::slug($title, '-');

        return [
            'user_id' => $users_id[array_rand($users_id, 1)],
            'title' => $title,
            'slug' => $slug,
            'description' => fake()->paragraph(),
            'category_id' => $categories_id[array_rand($categories_id, 1)],
        ];
    }
}
