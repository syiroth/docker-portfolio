<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Category;
use App\Models\Post;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    // 1. bisa dicegah di controller function, check permission dan rolenya dan semua atribut policy. seperti biasanya. best practice
    // 2. bisa dibuat middleware dengan generate token, artinya semua roles atau permission taruh di token tsb
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // buat roles admin dan writer
        DB::transaction(function(){
            // $admin_role = Role::where('name', 'admin')->firstOrFail();
            // $writer_role = Role::where('name', 'writer')->firstOrFail();

            // $admin_role->users()->attach(1);
            // $writer_role->users()->attach([1, 2]);
        });
        // Post::factory()->count(20)->create();
    }
}
